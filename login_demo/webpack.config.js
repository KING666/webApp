var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
// 引入css 单独打包插件
var ExtractTextPlugin = require("extract-text-webpack-plugin");
// 设置生成css 的路径和文件名，会自动将对应entry入口js文件中引入的CSS抽出成单独的文件
var packCSS = new ExtractTextPlugin('./css/[name].min.css');

//定义了一些文件夹的路径
var ROOT_PATH = path.resolve(__dirname);
var SRC_PATH = path.resolve(ROOT_PATH, 'src');
var BUILD_PATH = path.resolve(ROOT_PATH, 'build');
module.exports = {
    context: path.resolve(__dirname, 'src/js'),
	entry: {
        waplogin: SRC_PATH + '/js/waplogin.js' //入口文件地址
	},
	output: {
		path: BUILD_PATH,
		filename: 'js/[name].js', // [name]是一个变量，会自动替换成entry的key  js/[name].[hash:8].js
	},
	devServer: {
		historyApiFallback: true,
    	hot: true,
    	inline: true,
    	compress: true,
        host: '172.28.112.140',
		port:8089
	},
	module: {
		loaders: [{
				test: /\.css$/,
            	//css-loader?importLoaders=1!postcss-loader 增加postcss不能自动打包import的样式 暂时没找到原因
            	loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader',publicPath: '../' })
			}, {
				test: /\.(png|jpg|jpeg|gif)$/,
				loader: 'url-loader?limit=10000&name=images/[name].[ext]'
			}, {
            test: /\.(ttf|svg|woff|woff2|eot)$/,
            loader: 'url-loader?limit=10000&name=fonts/[name].[ext]'
        }]
	},
  
  plugins: [
      packCSS
  ]
};