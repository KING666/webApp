require('../css/waplogin.css');
require('./component/zepto.min.js');
var wapLogin = {
    init: function () {
        this.initDom();
        this.initEvent();
    },
    initDom: function () {
        this.$loginwarp = $(".login-wrap");
        this.windowHeight = $(window).height();
        this.windowWidth = $(window).width();
    },
    initEvent: function () {
        this.$loginwarp.css("min-height", this.windowHeight+"px");
        this.$loginwarp.css("max-width", this.windowWidth+"px");
    },
    submit:function () {
        var $cardId=$('#cardId');
        var cardId=$cardId.val();
        this.$tip=$('#tip');
        var match = /^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/;
        if(!match.test(cardId)){
            this.$tip.show();
            this.$tip.html('请输入合法的身份证号');
            $cardId.focus();
            return;
        }
        $('.card').show();
        $('.card-m').show();
        this.renderChart();


    },
    cancel:function () {
        this.$search.find('input').val('');
    }
};
wapLogin.init();