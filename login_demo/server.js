/**
 * Created by wangyl1 on 2017/12/5.
 * 启动node服务
 */

var express = require('express');
var http = require('http');
var path = require('path');

var app = express();
app.set('port', 8888);
app.use(express.static(path.join(__dirname, 'build')));

app.listen(app.get('port'), function(){
    console.log('静态资源服务器已启动,监听端口:' + app.get('port'));
});

